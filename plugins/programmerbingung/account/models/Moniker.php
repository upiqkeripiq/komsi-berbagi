<?php namespace Programmerbingung\Account\Models;

use Model;

/**
 * Moniker Model
 */
class Moniker extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'programmerbingung_account_monikers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => 'Programmerbingung\Account\Models\User'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}