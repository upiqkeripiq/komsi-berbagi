<?php namespace Programmerbingung\Account\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('programmerbingung_account_users', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            $table->date('dob');
            $table->string('email', 255);
            $table->string('phone', 20);
            $table->enum('gender', ['m', 'f']);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('programmerbingung_account_users');
    }

}
