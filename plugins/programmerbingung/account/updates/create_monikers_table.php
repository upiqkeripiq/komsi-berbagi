<?php namespace Programmerbingung\Account\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateMonikersTable extends Migration
{

    public function up()
    {
        Schema::create('programmerbingung_account_monikers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name', 255);
            $table->date('dob');
            $table->string('email', 255);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('programmerbingung_account_monikers');
    }

}
