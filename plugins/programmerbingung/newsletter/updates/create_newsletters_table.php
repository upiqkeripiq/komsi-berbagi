<?php namespace Programmerbingung\Newsletter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateNewslettersTable extends Migration
{

    public function up()
    {
        Schema::create('programmerbingung_newsletter_newsletters', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 255);
            $table->text('body');
            $table->timestamp('scheduled_at');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('programmerbingung_newsletter_newsletters');
    }

}
