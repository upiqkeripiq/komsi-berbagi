<?php namespace Programmerbingung\Newsletter\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Newsletter Back-end Controller
 */
class Newsletter extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Programmerbingung.Newsletter', 'newsletter', 'newsletter');
    }
}