<?php namespace Programmerbingung\Newsletter;

use System\Classes\PluginBase;

/**
 * newsletter Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'newsletter',
            'description' => 'No description provided yet...',
            'author'      => 'programmerbingung',
            'icon'        => 'icon-leaf'
        ];
    }

}
