<?php namespace Programmerbingung\Transaction\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTransactionsTable extends Migration
{

    public function up()
    {
        Schema::create('programmerbingung_transaction_transactions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->smallInteger('type_id');
            $table->integer('balance_record_id');
            $table->integer('moniker_id');
            $table->double('amount', 10, 2);
            $table->timestamp('transaction_date');
            # $table->time('transaction_time');
            $table->text('description');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('programmerbingung_transaction_transactions');
    }

}
