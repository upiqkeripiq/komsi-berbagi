<?php namespace Programmerbingung\Transaction\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateBalanceRecordsTable extends Migration
{

    public function up()
    {
        Schema::create('programmerbingung_transaction_balance_records', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->double('credit', 10, 2);
            $table->double('debit', 10, 2);
            $table->double('saldo', 10, 2);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('programmerbingung_transaction_balance_records');
    }

}
