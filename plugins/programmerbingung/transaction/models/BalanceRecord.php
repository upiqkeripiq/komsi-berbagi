<?php namespace Programmerbingung\Transaction\Models;

use Model;

/**
 * BalanceRecord Model
 */
class BalanceRecord extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'programmerbingung_transaction_balance_records';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'transaction' => 'Programmerbingung\Transaction\Models\Transaction'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}